/*
 * Copyright (c) 2015-2025 Phoinex Scholars Co. http://dpq.co.ir
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';

angular.module('ngMaterialDashboardCollection', [ //
    'ngMaterialDashboard',//
]);

/*
 * Copyright (c) 2015-2025 Phoinex Scholars Co. http://dpq.co.ir
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';

angular.module('ngMaterialDashboardCollection')
/**
 * 
 */
.config(function($routeProvider) {
	$routeProvider//
	.when('/collections', {
		controller : 'AmdCollectionsCtrl',
		templateUrl : 'views/amd-collections.html',
		name : 'Collections',
		icon : 'storage',
		navigate : true,
		protect: true,
	})//
	.when('/collections/new', {
		controller : 'AmdCollectionNewCtrl',
		templateUrl : 'views/amd-collection-new.html',
		protect: true,
	})//
	.when('/collections/:collectionId/documents/:documentId', {
		controller : 'AmdDocumentCtrl',
		templateUrl : 'views/amd-document.html',
		protect: true,
	})//
	.when('/collections/:id', {
		controller : 'AmdCollectionCtrl',
		templateUrl : 'views/amd-collection.html',
		protect: true,
	});
});

/*
 * Copyright (c) 2015-2025 Phoinex Scholars Co. http://dpq.co.ir
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

'use strict';

angular.module('ngMaterialDashboardCollection')

/**
 * @ngdoc controller
 * @name AmdBankGates
 * @description Manages bank backends
 * 
 */
.controller('AmdCollectionNewCtrl', function($scope, /*$collection, */$navigator) {

	var ctrl = {
			state: 'ok',
	};

	/**
	 * Adds new topic
	 * @param model
	 * @returns
	 */
	function add(model){
		return $collection.newCollection(model)//
		.then(function(collection){
			$navigator.openPage('collections/'+collection.id);
		});
	}

	/**
	 * Go to topics list.
	 * 
	 * @returns
	 */
	function cancel(){
		$navigator.openPage('collections');
	}

	$scope.cancel = cancel;
	$scope.add = add;
	$scope.ctrl = ctrl;
});

'use strict';

angular.module('ngMaterialDashboardCollection')

/**
 * @ngdoc controller
 * @name AmdBankGates
 * @description Manages bank backends
 * 
 */
.controller('AmdCollectionCtrl', function($scope, $navigator, /*$collection, */$routeParams, QueryParameter) {

	var paginatorParameter = new QueryParameter();
	paginatorParameter.setOrder('id', 'd');
	var requests = null;
	var ctrl = {
			state: 'ok',
			items: []
	};

	/**
	 * جستجوی درخواست‌ها
	 * 
	 * @param paginatorParameter
	 * @returns
	 */
	function find(query) {
		paginatorParameter.setQuery(query);
		paginatorParameter.setPage(0);
		reload();
	}

	/**
	 * لود کردن داده‌های صفحه بعد
	 * 
	 * @returns
	 */
	function nextPage() {
		if (ctrl.status === 'working') {
			return;
		}
		if (requests && !requests.hasMore()) {
			return;
		}
		if (requests) {
			paginatorParameter.setPage(requests.next());
		}
		// start state (device list)
		ctrl.status = 'working';
		$scope.collection.documents(paginatorParameter)//
		.then(function(items) {
			requests = items;
			ctrl.items = ctrl.items.concat(requests.items);
			ctrl.status = 'ok';
		}, function(error) {
			ctrl.error = error;
			ctrl.status = 'fail';
		});
	}

	/**
	 * Adds new chiled
	 * @returns
	 */
	function addDocument(){
		$scope.collection.newDocument();
		reload();
	}


	/**
	 * Loads collection
	 * 
	 * @returns
	 */
	function reload(){
		$collection.collection($routeParams.id)//
		.then(function(collection){
			$scope.collection = collection;
			requests = null;
			ctrl.items = [];
			nextPage();
		}, function(error){
			$scope.collection = null;
			ctrl.status = 'notFound';
			ctrl.error = error;
		});
	}

	/**
	 * درخواست مورد نظر را از سیستم حذف می‌کند.
	 * 
	 * @param request
	 * @returns
	 */
	function remove(object) {
		confirm('delete item ' + object.id +'?')//
		.then(function(){
			return object.delete();//
		})//
		.then(function(){
			// TODO: maso, 1395: go to the model page
			var index = ctrl.items.indexOf(object);
			if (index > -1) {
				ctrl.items.splice(index, 1);
			}
			$location.path('/collection/' + $scope.collection.id);
		}, function(error){
			alert('fail to delete item:' + error.message);
		});

	}

	function removeCollection(){
		confirm('delete collection ' + $scope.collection.id +'?')//
		.then(function(){
		    return $scope.collection.delete()//
		})//
		.then(function(){
			$navigator.openPage('collections');
		}, function(error){
			alert('Fail to remove collection:' + error.data.message);
		});
	}

	$scope.items = [];
	$scope.reload = reload;
	$scope.search = find;
	$scope.nextPage = nextPage;

	$scope.remove = remove;
	$scope.removeCollection = removeCollection;
	$scope.add = addDocument;

	$scope.ctrl = ctrl;


	// Pagination toolbar
	$scope.paginatorParameter = paginatorParameter;
	$scope.reload = reload;
	$scope.sortKeys= [
		'id', 
		'name',
		'description'
		];
	$scope.moreActions=[{
		title: 'New document',
		icon: 'add',
		action: addDocument
	}];
});


'use strict';

angular.module('ngMaterialDashboardCollection')

/**
 * @ngdoc controller
 * @name AmdBankGates
 * @description Manages bank backends
 * 
 */
.controller('AmdCollectionsCtrl', function($scope, /*$collection, */QueryParameter, $navigator ) {

	var paginatorParameter = new QueryParameter();
	paginatorParameter.setOrder('id', 'd');
	var requests = null;
	var ctrl = {
			state: 'ok',
			items: []
	};

	/**
	 * Loading collection list
	 * 
	 * @returns
	 */
	function nextPage() {
		if (ctrl.status === 'working') {
			return;
		}
		if (requests && !requests.hasMore()) {
			return;
		}
		if (requests) {
			paginatorParameter.setPage(requests.next());
		}
		// start state (device list)
		ctrl.status = 'working';
		$collection.collections(paginatorParameter)//
		.then(function(items) {
			requests = items;
			ctrl.items = ctrl.items.concat(requests.items);
			ctrl.status = 'ok';
		}, function() {
			ctrl.status = 'fail';
		});
	}


	/**
	 * درخواست مورد نظر را از سیستم حذف می‌کند.
	 * 
	 * @param request
	 * @returns
	 */
	function remove(pobject) {
		confirm('delete collection ' + object.id +'?')//
		.then(function(){
		   return pobject.delete();
		})//
		.then(function(){
			var index = ctrl.items.indexOf(pobject);
			if (index > -1) {
				ctrl.items .splice(index, 1);
			}
			$location.path('collections');
		}, function(error){
		    alert('fail to delete collection:' + error.message);
		});
	}

	/**
	 * تمام حالت‌های کنترل ررا بدوباره مقدار دهی می‌کند.
	 * 
	 * @returns
	 */
	function reload(){
		requests = null;
		ctrl.items = [];
		nextPage();
	}

	function addCollection(){
		$navigator.openPage('collections/new');
	}
	
	/*
	 * تمام امکاناتی که در لایه نمایش ارائه می‌شود در اینجا نام گذاری
	 * شده است.
	 */
	$scope.items = [];
	$scope.nextPage = nextPage;
	$scope.remove = remove;
	$scope.addCollection = addCollection;
	$scope.ctrl = ctrl;

	// Pagination
	$scope.paginatorParameter = paginatorParameter;
	$scope.reload = reload;
	$scope.sortKeys= [
		'id', 'creation_dtime'
		];
	$scope.moreActions=[{
		title: 'New collection',
		icon: 'add',
		action: addCollection
	}];

});

'use strict';

angular.module('ngMaterialDashboardCollection')

/**
 * @ngdoc controller
 * @name AmdBankGates
 * @description Manages bank backends
 * 
 */
.controller('AmdDocumentCtrl', function($scope, $navigator, $location, /*$collection, */$routeParams) {

	var ctrl = {
			state: 'ok',
			items: []
	};



	/**
	 * Loads collection
	 * 
	 * @returns
	 */
	function reload(){
		ctrl.status = 'working';
		$collection.collection($routeParams.collectionId)//
		.then(function(collection){
			$scope.collection = collection;
			return collection.document($routeParams.documentId);
		}, function(error){
			$scope.collection = null;
			ctrl.status = 'notFound';
			ctrl.error = error;
		})//
		.then(function(document){
			$scope.document = document;
			$scope.keys=[];
			for(var key in $scope.document){
				if(!angular.isFunction($scope.document[key])){
					$scope.keys.push(key);
				}
			}
		})//
		.finally(function(){
			ctrl.status = 'ok';
		});
	}
	
	/**
	 * Adding new key
	 * 
	 * @returns
	 */
	function addKey(){
		prompt('Enter new key:')//
		.then(function(key){
			$scope.keys.push(key);
		});
	}
	
	/**
	 * Removes key and value
	 * 
	 * @param key
	 * @returns
	 */
	function removeKey(key){
		$scope.document[key] = '';
		var index = $scope.keys.indexOf(key);
		if (index > -1) {
			$scope.keys.splice(index, 1);
		}
	}
	
	function saveDocument(){
		$scope.document.update()//
		.then(function(){
			toast('Document saved');
		}, function(error){
			alert('Fail to save document:' + error.data.message);
		});
	}
	
	function deleteDocument(){
		confirm('delete document ' + $scope.document.id +'?')//
		.then(function(){
		    return $scope.document.remove()//
		})//
		.then(function(){
			$location.path('collections/' + $scope.collection.id);
		}, function(error){
			alert('Fail to remove document:' + error.data.message);
		});
	}

	$scope.saveDocument = saveDocument;
	$scope.deleteDocument = deleteDocument;
	$scope.addKey = addKey;
	$scope.removeKey = removeKey;
	$scope.reload = reload;
	$scope.ctrl = ctrl;
	
//	reload();
});


angular.module('ngMaterialDashboardCollection').run(['$templateCache', function($templateCache) {
  'use strict';

  $templateCache.put('views/amd-collection-new.html',
    "<form layout=column ng-submit=add(model) mb-preloading=\"ctrl.status === 'working'\" layout-padding flex> <input hide type=\"submit\"> <div layout=column> <md-input-container> <label translate>Name</label> <input ng-model=model.name required> </md-input-container> <md-input-container> <label translate>Title</label> <input ng-model=model.title> </md-input-container> <md-input-container> <label translate>Description</label> <input ng-model=model.description> </md-input-container> </div> <div class=md-toolbar-tools> <span flex></span> <md-button ng-click=add(model)> <wb-icon aria-label=close>done</wb-icon> <span translate>Add</span> </md-button> <md-button ng-click=cancel()> <wb-icon aria-label=close>close</wb-icon> <span translate>Cancle</span> </md-button> </div> </form>"
  );


  $templateCache.put('views/amd-collection.html',
    "<md-content layout=column layout-padding flex> <div layout=column class=md-whiteframe-2dp> <div style=\"min-height: 160px\" layout=row> <table> <tr><td translate=\"\">Name</td><td>: {{collection.name}}</td></tr> <tr><td translate=\"\">Title</td><td>: {{collection.title}}</td></tr> <tr><td translate=\"\">Description</td><td><p>:{{collection.description}}</p></td></tr>  </table> </div> <div layout=row> <span flex></span> <md-button ng-click=removeCollection()> <wb-icon>delete</wb-icon> <span translate=\"\">delete</span> </md-button> </div> </div>  <mb-pagination-bar mb-model=paginatorParameter mb-reload=reload mb-sort-keys=sortKeys mb-more-actions=moreActions> </mb-pagination-bar> <md-list flex> <md-list-item ng-repeat=\"object in ctrl.items track by object.id\" ng-href=\"{{'collections/'+collection.id+'/documents/'+object.id}}\" class=md-3-line> <img ng-src={{object.cover}} class=md-avatar alt=\"{{item.who}}\"> <div class=md-list-item-text layout=column> <h3>{{object.id}}</h3>   </div> </md-list-item> </md-list> <div layout=column layout-align=\"center center\" ng-show=\"!ctrl.items || ctrl.items.length == 0\"> <h2>Empty part list</h2> <p>Any part match with the query. You can add a part by click on the add button.</p> <md-button ng-click=add()> <wb-icon>add</wb-icon> <span translate=\"\">Add</span> </md-button> </div> </md-content>"
  );


  $templateCache.put('views/amd-collections.html',
    "<div wb-infinate-scroll=nextPage layout=column flex>  <mb-pagination-bar mb-model=paginatorParameter mb-reload=reload mb-sort-keys=sortKeys mb-more-actions=moreActions> </mb-pagination-bar> <md-list> <md-list-item ng-repeat=\"object in ctrl.items track by object.id\" ng-href=\"{{'collections/'+object.id}}\" class=md-3-line> <wb-icon>storage</wb-icon> <div class=md-list-item-text layout=column> <h3>{{object.title}}</h3> <h4>{{object.name}}</h4> <p>{{object.description}}</p> </div> <md-divider md-inset></md-divider> </md-list-item> </md-list> <div layout=column layout-align=\"center center\" ng-show=\"!ctrl.items || ctrl.items.length == 0\"> <h2>Empty collection list</h2> <p>Any collection match with the query. You can add a new collection by click on the add button. Collections are a place to store data.</p> <md-button ng-click=addCollection()> <wb-icon>add</wb-icon> <span translate=\"\">Add</span> </md-button> </div> </div>"
  );


  $templateCache.put('views/amd-document.html',
    "<div layout=column flex> <md-toolbar> <div class=md-toolbar-tools> <span flex></span> <md-button class=md-icon-button aria-label=Add ng-click=addKey()> <wb-icon>add</wb-icon> </md-button> <md-button class=md-icon-button aria-label=Reload ng-click=reload()> <wb-icon>repeat</wb-icon> </md-button> <md-button class=md-icon-button aria-label=Save ng-click=saveDocument()> <wb-icon>save</wb-icon> </md-button> <md-button class=md-icon-button aria-label=Delete ng-click=deleteDocument()> <wb-icon>delete</wb-icon> </md-button> </div> </md-toolbar> <div ng-repeat=\"key in keys\" layout=row> <md-input-container flex> <label translate>{{key}}</label> <input ng-model=document[key]> </md-input-container> <md-button ng-click=removeKey(key)> <wb-icon>delete</wb-icon> </md-button> </div> </div>"
  );

}]);
