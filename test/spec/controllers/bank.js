'use strict';

describe('Controller AmdCollection', function() {

	// load the controller's module
	beforeEach(module('ngMaterialDashboardCollection'));

	var AmdBankCtrl;
	var scope;

	// Initialize the controller and a mock scope
	beforeEach(inject(function($controller, $rootScope, _$usr_) {
		scope = $rootScope.$new();
		AmdBankCtrl = $controller('AmdCollectionsCtrl', {
			$scope : scope,
			$usr : _$usr_
			// place here mocked dependencies
		});
	}));

	it('should be defined', function() {
		expect(angular.isDefined(AmdBankCtrl)).toBe(true);
	});
});
