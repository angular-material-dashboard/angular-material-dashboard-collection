angular.module('ngMaterialDashboardCollection').run(['$templateCache', function($templateCache) {
  'use strict';

  $templateCache.put('views/amd-collection-new.html',
    "<form layout=column ng-submit=add(model) mb-preloading=\"ctrl.status === 'working'\" layout-padding flex> <input hide type=\"submit\"> <div layout=column> <md-input-container> <label translate>Name</label> <input ng-model=model.name required> </md-input-container> <md-input-container> <label translate>Title</label> <input ng-model=model.title> </md-input-container> <md-input-container> <label translate>Description</label> <input ng-model=model.description> </md-input-container> </div> <div class=md-toolbar-tools> <span flex></span> <md-button ng-click=add(model)> <wb-icon aria-label=close>done</wb-icon> <span translate>Add</span> </md-button> <md-button ng-click=cancel()> <wb-icon aria-label=close>close</wb-icon> <span translate>Cancle</span> </md-button> </div> </form>"
  );


  $templateCache.put('views/amd-collection.html',
    "<md-content layout=column layout-padding flex> <div layout=column class=md-whiteframe-2dp> <div style=\"min-height: 160px\" layout=row> <table> <tr><td translate=\"\">Name</td><td>: {{collection.name}}</td></tr> <tr><td translate=\"\">Title</td><td>: {{collection.title}}</td></tr> <tr><td translate=\"\">Description</td><td><p>:{{collection.description}}</p></td></tr>  </table> </div> <div layout=row> <span flex></span> <md-button ng-click=removeCollection()> <wb-icon>delete</wb-icon> <span translate=\"\">delete</span> </md-button> </div> </div>  <mb-pagination-bar mb-model=paginatorParameter mb-reload=reload mb-sort-keys=sortKeys mb-more-actions=moreActions> </mb-pagination-bar> <md-list flex> <md-list-item ng-repeat=\"object in ctrl.items track by object.id\" ng-href=\"{{'collections/'+collection.id+'/documents/'+object.id}}\" class=md-3-line> <img ng-src={{object.cover}} class=md-avatar alt=\"{{item.who}}\"> <div class=md-list-item-text layout=column> <h3>{{object.id}}</h3>   </div> </md-list-item> </md-list> <div layout=column layout-align=\"center center\" ng-show=\"!ctrl.items || ctrl.items.length == 0\"> <h2>Empty part list</h2> <p>Any part match with the query. You can add a part by click on the add button.</p> <md-button ng-click=add()> <wb-icon>add</wb-icon> <span translate=\"\">Add</span> </md-button> </div> </md-content>"
  );


  $templateCache.put('views/amd-collections.html',
    "<div wb-infinate-scroll=nextPage layout=column flex>  <mb-pagination-bar mb-model=paginatorParameter mb-reload=reload mb-sort-keys=sortKeys mb-more-actions=moreActions> </mb-pagination-bar> <md-list> <md-list-item ng-repeat=\"object in ctrl.items track by object.id\" ng-href=\"{{'collections/'+object.id}}\" class=md-3-line> <wb-icon>storage</wb-icon> <div class=md-list-item-text layout=column> <h3>{{object.title}}</h3> <h4>{{object.name}}</h4> <p>{{object.description}}</p> </div> <md-divider md-inset></md-divider> </md-list-item> </md-list> <div layout=column layout-align=\"center center\" ng-show=\"!ctrl.items || ctrl.items.length == 0\"> <h2>Empty collection list</h2> <p>Any collection match with the query. You can add a new collection by click on the add button. Collections are a place to store data.</p> <md-button ng-click=addCollection()> <wb-icon>add</wb-icon> <span translate=\"\">Add</span> </md-button> </div> </div>"
  );


  $templateCache.put('views/amd-document.html',
    "<div layout=column flex> <md-toolbar> <div class=md-toolbar-tools> <span flex></span> <md-button class=md-icon-button aria-label=Add ng-click=addKey()> <wb-icon>add</wb-icon> </md-button> <md-button class=md-icon-button aria-label=Reload ng-click=reload()> <wb-icon>repeat</wb-icon> </md-button> <md-button class=md-icon-button aria-label=Save ng-click=saveDocument()> <wb-icon>save</wb-icon> </md-button> <md-button class=md-icon-button aria-label=Delete ng-click=deleteDocument()> <wb-icon>delete</wb-icon> </md-button> </div> </md-toolbar> <div ng-repeat=\"key in keys\" layout=row> <md-input-container flex> <label translate>{{key}}</label> <input ng-model=document[key]> </md-input-container> <md-button ng-click=removeKey(key)> <wb-icon>delete</wb-icon> </md-button> </div> </div>"
  );

}]);
